<?php

if (php_sapi_name() == "cli") {
  require "functions.php";

  echo "Executing cron\n";

  $prospectsNumber = $bdd->query("SELECT COUNT(*) as numberOfProspects FROM landingMails WHERE treated = 0")->fetch()["numberOfProspects"];

  if($prospectsNumber > 5){
    echo "Too much mail. Not sendingi\n";
    // On envoie pas sarace
    $mail          = newMailInit();
    $mail->setFrom('noreply@nebulae.co', 'Robot d\'envoie de mail');
    $mail->addAddress("recrutement@list.nebulae.co");

    $mail->Subject = "Erreur lors de l'envoie automatique des mails du jour";
    $mail->Body    = 
    `Yo !<br>
    J'ai détécté plus de 5 nouvelles personnes sur les dernières 24h. J'ai pas envoyé les mails du coup (pour éviter les spams). <br>
    Tu peux aller checker sur le portail pour vérifier et régulariser ça stp ? Sinon je vais devoir te le redire demail :/<br>
    <br>
    Bisous`;
    $mail->AltBody = strip_tags(`Yo !<br>
    J'ai détécté plus de 5 nouvelles personnes sur les dernières 24h. J'ai pas envoyé les mails du coup (pour éviter les spams). <br>
    Tu peux aller checker sur le portail pour vérifier et régulariser ça stp ? Sinon je vais devoir te le redire demail :/<br>
    <br>
    Bisous`);

    if($mail->send()) {
      $update = $bdd->prepare("UPDATE landingMails SET treated = 1 WHERE id = :id");
      $update->execute(array(
        'id' => $_GET["send"]
      ));
    }    

  }else{
    $onboarding = $bdd->query("SELECT * FROM contents WHERE `name` = 'onboarding' ORDER BY `version` DESC LIMIT 1")->fetch();
    $onboardingText = htmlspecialchars_decode($onboarding['content']);
    $configEmail = $bdd->query("SELECT * FROM configs WHERE `config_key` = 'landing_mail_sender'")->fetch();
    $onboardingEmail = $configEmail['config_value'];
    $configName = $bdd->query("SELECT * FROM configs WHERE `config_key` = 'landing_name_sender'")->fetch();
    $onboardingName = $configName['config_value'];
    $configSubject = $bdd->query("SELECT * FROM configs WHERE `config_key` = 'landing_subject'")->fetch();
    $onboardingSubject = $configSubject['config_value'];
    $configCC = $bdd->query("SELECT * FROM configs WHERE `config_key` = 'landing_cc'")->fetch();
    $onboardingCC = $configCC['config_value'];

    $prospects = $bdd->query("SELECT DATE_FORMAT(inscription_date, '%d/%m/%Y %Hh%i') AS the_date, email, id, treated FROM landingMails WHERE treated = 0");
    while($prospect = $prospects->fetch()){
      $mail          = newMailInit();
      $mail->setFrom('noreply@nebulae.co', 'Nebulae');
      $mail->addAddress($prospect["email"]);
      $mail->addReplyTo($onboardingEmail, $onboardingName);
      $mail->addCC($onboardingCC);

      $mail->Subject = $onboardingSubject;
      $mail->Body    = $onboardingText;
      $mail->AltBody = strip_tags($onboardingText);

      if($mail->send()) {
	echo "Mail sent to ".$prospect['email']."\n";
        $update = $bdd->prepare("UPDATE landingMails SET treated = 1 WHERE id = :id");
        $update->execute(array(
          'id' => $prospect["id"]
        ));
      }
    }
  }





} else {
  header("Location: index.php");
}

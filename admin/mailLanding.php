<?php
    session_start();
    if($_SESSION["user"]["gidnumber"][0] != 501){
      header("Location: ../index.php");
    }

    require "../functions.php";


    // Form sended
    if(isset($_POST["test"])){
      $onboardingText    = $_POST["content"];
      $onboardingEmail   = $_POST["senderEmail"];
      $onboardingName    = $_POST["senderName"];
      $onboardingCC      = $_POST["ccMail"];
      $onboardingSubject = $_POST["mailSubject"];

      $userEmailForTest  = $_SESSION["user"]["mail"][0];

      $_SESSION["flash"]["warning"] = "Attention, les modifications ne sont pas encore enregistrées en base de donnée !";

      $mail = newMailInit();
      $mail->setFrom('noreply@nebulae.co', 'Nebulae');
      $mail->addAddress($userEmailForTest);

      $mail->Subject = '[TEST] '. $onboardingSubject;
      $mail->Body    = $onboardingText;
      $mail->AltBody = strip_tags($onboardingText);
    
      if(!$mail->send()) {
        $_SESSION["flash"]["error"]   = "Erreur lors de l'envoie du mail: " . $mail->ErrorInfo ;
      } else {
        $_SESSION["flash"]["success"] = "Je viens de t'envoyer le mail à l'adresse ". $userEmailForTest;
      }
    }elseif(isset($_POST["content"]) && isset($_POST["token"]) && hash_equals($token, $_POST["token"])){
      $updateContents = $bdd->prepare("UPDATE `contents` SET content = :content, `date` = NOW() WHERE `name` = 'onboarding'");
      $updateContents->execute(array(
        'content' => htmlspecialchars($_POST["content"])
      ));

      $updateSenderName = $bdd->prepare("UPDATE configs SET `config_value` = :sendername WHERE config_key = 'landing_name_sender'");
      $updateSenderName->execute(array(
        'sendername' => $_POST['senderName']
      ));

      $updateSenderEmail = $bdd->prepare("UPDATE configs SET `config_value` = :sendermail WHERE config_key = 'landing_mail_sender'");
      $updateSenderEmail->execute(array(
        'sendermail' => $_POST['senderEmail'], 
      ));

      $updateMailSubject = $bdd->prepare("UPDATE configs SET `config_value` = :subject WHERE config_key = 'landing_subject'");
      $updateMailSubject->execute(array(
        'subject' => $_POST['mailSubject'], 
      ));

      $updateMailCC = $bdd->prepare("UPDATE configs SET `config_value` = :cc WHERE config_key = 'landing_cc'");
      $updateMailCC->execute(array(
        'cc' => $_POST['ccMail'], 
      ));
    }


    // Specific GET functions (delete & toggle)
    if(isset($_GET["delete"]) && isset($_GET["token"]) && hash_equals($token, $_GET["token"])){
      $delete = $bdd->prepare("DELETE FROM `landingMails` WHERE id = :id");
      $delete->execute(array(
        'id' => $_GET["delete"]
      ));
    }

    if(isset($_GET["toggle"]) && isset($_GET["token"]) && hash_equals($token, $_GET["token"])){
      $toggle = $bdd->prepare("UPDATE `landingMails` SET treated = !treated WHERE id = :id");
      $toggle->execute(array(
        'id' => $_GET["toggle"]
      ));
    }

    // Get data from database
    if(!isset($onboardingText)){
      $onboarding = $bdd->query("SELECT * FROM contents WHERE `name` = 'onboarding' ORDER BY `version` DESC LIMIT 1")->fetch();
      $onboardingText = htmlspecialchars_decode($onboarding['content']);
    }
    if(!isset($onboardingEmail)){
      $configEmail = $bdd->query("SELECT * FROM configs WHERE `config_key` = 'landing_mail_sender'")->fetch();
      $onboardingEmail = $configEmail['config_value'];
    }
    if(!isset($onboardingName)){
      $configName = $bdd->query("SELECT * FROM configs WHERE `config_key` = 'landing_name_sender'")->fetch();
      $onboardingName = $configName['config_value'];
    }
    if(!isset($onboardingSubject)){
      $configSubject = $bdd->query("SELECT * FROM configs WHERE `config_key` = 'landing_subject'")->fetch();
      $onboardingSubject = $configSubject['config_value'];
    }

    if(!isset($onboardingCC)){
      $configCC = $bdd->query("SELECT * FROM configs WHERE `config_key` = 'landing_cc'")->fetch();
      $onboardingCC = $configCC['config_value'];
    }

    // Manually Send
    if(isset($_GET["send"]) && isset($_GET["token"]) && hash_equals($token, $_GET["token"])){
      $select = $bdd->prepare("SELECT * FROM `landingMails` WHERE id = :id");
      $select->execute(array(
        'id'  => $_GET["send"]
      ));
      $recipient      = $select->fetch()["email"];
      
      $mail          = newMailInit();
      $mail->setFrom('noreply@nebulae.co', 'Nebulae');
      $mail->addAddress($recipient);
      $mail->addReplyTo($onboardingEmail, $onboardingName);
      $mail->addCC($onboardingCC);

      $mail->Subject = $onboardingSubject;
      $mail->Body    = $onboardingText;
      $mail->AltBody = strip_tags($onboardingText);
    
      if(!$mail->send()) {
        $_SESSION["flash"]["error"] = "Erreur lors de l'envoie du mail:" . $mail->ErrorInfo ;
        header('Location: /admin/mailLanding.php');
        die();
      } else {
        $_SESSION["flash"]["success"] = "Message sent to ".$recipient;
        $update = $bdd->prepare("UPDATE landingMails SET treated = 1 WHERE id = :id");
        $update->execute(array(
          'id' => $_GET["send"]
        ));
        header('Location: /admin/mailLanding.php');
        die();
      }
    }

    $prospects = $bdd->query("SELECT DATE_FORMAT(inscription_date, '%d/%m/%Y %Hh%i') AS the_date, email, id, treated FROM landingMails ORDER BY treated ASC, inscription_date DESC");

?>
<!DOCTYPE html>
<html lang="">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Mail post inscription sur la landing page - The Nebulae - Portail</title>
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="/css/bootstrap.min.css">
		<link rel="stylesheet" href="/css/portal.css">
		<link rel="stylesheet" href="/css/trix.css">
	</head>
	<body>
    <!-- Fixed navbar -->
    <?php require("../menu.php"); ?>

    <div class="container">
      <div class="container main-cont">
          <h2>Suivi des inscription landing page</h2>

          <?php

          if(isset($_SESSION["flash"]["warning"])){
          ?>
            <div class="alert alert-warning" role="alert"><?= $_SESSION["flash"]["warning"]; ?></div>
          <?php
            unset($_SESSION["flash"]["warning"]);
          }

          if(isset($_SESSION["flash"]["error"])){
          ?>
            <div class="alert alert-danger" role="alert"><?= $_SESSION["flash"]["error"]; ?></div>
          <?php
            unset($_SESSION["flash"]["error"]);
          }

          if(isset($_SESSION["flash"]["success"])){
          ?>
            <div class="alert alert-success" role="alert"><?= $_SESSION["flash"]["success"]; ?></div>
          <?php
            unset($_SESSION["flash"]["success"]);
          }

          ?>
          
          <!--
          <p>
            Améliorations: <br>
            - Versionning du mail (actif / inactif)<br>
            - Reglage de la date / heure d'envoie ?<br>
            - Envoie automatique everyday at 6pm + check if num mail > 5 => don't sendit and mail recru@nebu to prevent issues
          </p>-->

          <p>
            Le tableau ci-dessous montre l'ensemble des mails réceptionné sur la landing page. <br>
            La date correspond à la date à laquelle la personne à envoyé son email sur la landing page.<br>
            En cliquant sur la corbeille, tu supprimes le mail de la base de donnée (on ne peut pas le récuperer)<br>
            En cliquant sur la croix rouge, ou le tick vert, tu peux changer le statuts de contact manuellement (si tu as envoyé un mail par ailleur par exemple)<br>
            En cliquant sur l'avion en papier, tu déclanches l'envoie du mail à la personne. 
          </p>

          <table class="table table-striped">
            <thead>
              <tr>
                <th>Supprimer</th>
                <th>Email</th>
                <th>Date d'inscription</th>
                <th>On l'a déjà contacté ?</th>
                <th>Envoyer manuellement le mail</th>
              </tr>
            </thead>
            <tbody>
              <?php 
                while($prospect = $prospects->fetch()){
                ?>
                <tr>
                  <td>
                    <a href='?delete=<?= $prospect["id"];?>&amp;token=<?= $token;?>'><span class="glyphicon red glyphicon-trash" aria-hidden="true"></span></a>
                  </td>
                  <td><?= $prospect["email"];?></td>
                  <td><?= $prospect["the_date"]; ?></td>
                  <td>
                    <a href='?toggle=<?= $prospect["id"];?>&amp;token=<?= $token;?>'>
                      <span class="glyphicon <?= $prospect["treated"]? "green glyphicon-ok" : "red glyphicon-remove" ?>" aria-hidden="true"></span>
                    </a>  
                  </td>
                  <td>
                    <?php
                      if($prospect["treated"]){
                      ?>
                      <a onclick="return confirm('On a déjà envoyé un mail à cette adresse mail. Es-tu sur de vouloir lui renvoyer ?');" href="?send=<?= $prospect["id"];?>&amp;token=<?= $token;?>"><span class="glyphicon glyphicon-send" aria-hidden="true"></span></a>
                      <?php
                      }else{
                      ?>
                        <a href="?send=<?= $prospect["id"];?>&amp;token=<?= $token;?>"><span class="glyphicon glyphicon-send" aria-hidden="true"></span></a>
                      <?php
                      }
                    ?>
                    
                  </td>
                </tr>
                <?php
                }
              ?>
            </tbody>
          </table>    
          
          <form action="" method="POST">
            <div class="row">
              <div class="form-group col-md-6">
                <label for="senderEmail">Email reply-to</label>
                <input type="email" class="form-control" id="senderEmail" name="senderEmail" placeholder="Email" value="<?= $onboardingEmail;?>">

              </div>
              <div class="form-group col-md-6">
                <label for="senderName">Nom a afficher en reply-to</label>
                <input type="text" class="form-control" id="senderName" name="senderName" placeholder="Nom" value="<?= $onboardingName;?>">
              </div>
            </div>

            <div class="form-group">
              <label for="ccMail">Mail en copie (cc)</label>
              <input type="text" class="form-control" id="ccMail" name="ccMail" placeholder="CC" value="<?= $onboardingCC;?>">
            </div>

            <div class="form-group">
              <label for="mailSubject">Sujet du mail</label>
              <input type="text" class="form-control" id="mailSubject" name="mailSubject" placeholder="Sujet" value="<?= $onboardingSubject;?>">
            </div>

            <div class="form-group">
              <label for="content">Texte à envoyer</label>
              <input id="content" type="hidden" name="content" value="<?= htmlentities($onboardingText); ?>">
              <trix-editor input="content" id="content" rows="10" ></trix-editor>
            </div>
            <input type="hidden" name="version" value="<?= $onboarding["version"]; ?>">
            <input type="hidden" name="token" value="<?= $token; ?>">
            <button type="submit" class="btn btn-default">Mettre à jour le texte à envoyer</button>
            <button type="submit" name="test" value="1" class="btn btn-default">M'envoyer le mail pour vérifier</button>
          </form>

      </div>
    </div> <!-- /container -->

		
		<!-- jQuery --> 
		<script src="/js/jquery.min.js"></script>
		<!-- Bootstrap JavaScript -->
		<script src="/js/bootstrap.min.js"></script>
		<script src="/js/trix.js"></script>
	</body>
</html>


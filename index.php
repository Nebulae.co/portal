<?php
        session_start();
        if(empty($_SESSION["user"])){
                header("Location: login.php");
        }
?>
<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>The Nebulae - Portail</title>

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="/css/bootstrap.min.css">
        <link rel="stylesheet" href="/css/portal.css">
    </head>
    <body>
        <!-- Fixed navbar -->
        <?php require("menu.php"); ?>

        <div class="container main-cont">
            <div class="jumbotron">
                <h1>Portail Nebulae</h1>
                <p>	Bienvenue sur Nebulae! Merci d'être ici! <br>
                    Le compte que tu viens de créer sur ce portail te sera utile partout, c'est avec lui que tu t'authentifieras sur tous les services.<br>
                    Si tu as des questions, n'hésites pas à les poser (soit sur le <a href="https://chat.nebulae.co">chat</a>, soit par email)!
                </p>
            </div>
            <div id="services">
                <div class="row">
                    <div class="col-md-4">
                        <a href="https://mail.nebulae.co"><img src="/img/NebulaeMail.png" alt="Nebulae Mail"></a>
                        <p>Une adresse email respectueuse de la vie privée. Tu peux y accéder par le webmail (clic sur le logo), ou par n'importe quel application de mail (Thunderbird, le mail de ton téléphone, ...)</p>
                    </div>
                    <div class="col-md-4">
                        <a href="https://cloud.nebulae.co"><img src="/img/NebulaeBox.png" alt="Nebulae Box"></a>
                        <p>Nebulae Cloud: synchroniser tous vos fichier dans le nuage</p>
                    </div>
                    <div class="col-md-4">
                        <a href="https://chat.nebulae.co"><img src="/img/NebulaeChat.png" alt="Nebulae Chat"></a>
                        <p>C'est la plateforme de discussion et d'échange principale de Nebulae. Tu y trouveras des discussions de fond, de forme, des pleines et des creuses. Si tu veux t'investir dans la communauté c'est l'endroit ou commencer! Si tu veux simplement être au courant de ce qui se passe, c'est l'endroit ou il faut être!</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <a href="https://wiki.nebulae.co"><img src="/img/NebulaeWiki.png" alt="Nebulae Wiki"></a>
                        <p>Pour partager la connaissance autour de Nebulae! Il y a déjà quelques tutos pour utiliser les services. N'hésite pas à en rajouter, les modifier, les améliorer!</p>
                    </div>
                </div>
            </div>    	
        </div> <!-- /container -->
        <!-- jQuery --> 
        <script src="/js/jquery.min.js"></script>
        <!-- Bootstrap JavaScript -->
        <script src="/js/bootstrap.min.js"></script>
    </body>
</html>


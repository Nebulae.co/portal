<?php

require("credentials.php");

function random_salt($length) {
	$possible = '0123456789'.
		'abcdefghijklmnopqrstuvwxyz'.
		'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.
		'./';
	$str = '';
	mt_srand((double)microtime() * 1000000);
	while (strlen($str) < $length)
		$str .= substr($possible,(rand()%strlen($possible)),1);
	return $str;
}

function generate_password($password){
	$salt = random_salt(13);

	return "{CRYPT}".crypt($password, $salt);
}


try{
	$bdd = new PDO('mysql:host='.$dbHost.';dbname='.$dbName.';charset=utf8', $dbUser, $dbPassword);
}
catch(Exception $e){
	die('Erreur : '.$e->getMessage());
}


if (!isset($_SESSION['token'])) {
	$token = bin2hex(random_bytes(32));
	$_SESSION['token'] = $token;
	$_SESSION['token_time'] = time();
}
else
{
	$token = $_SESSION['token'];
}

function newMailInit(){
	require_once 'deps/PHPMailer/class.phpmailer.php';
	require_once 'deps/PHPMailer/class.smtp.php';

	$mail = new PHPMailer;
	$mail->SMTPDebug = 0;                               // Enable verbose debug output
	$mail->SMTPOptions = array(
		'ssl' => array(
				'verify_peer' => false,
				'verify_peer_name' => false,
				'allow_self_signed' => true
		)
	);
	$mail->isSMTP();                        // Set mailer to use SMTP
	$mail->Host     = SMTPHOST;            // Specify main and backup SMTP servers
	$mail->SMTPAuth = SMTPAUTH;            // Enable SMTP authentication
	$mail->Username = SMTPUSERNAME;        // SMTP username
	$mail->Password = SMTPPASSWORD;        // SMTP password
	$mail->charSet  = "UTF-8"; 
	return $mail;
}


?>
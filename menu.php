<nav class="navbar navbar-default navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand logo-header" href="/"><img src="/img/Nebulae.png" alt="Logo"></a>
    </div>
    <div id="navbar" class="collapse navbar-collapse">
      <ul class="nav navbar-nav">
        <li <?php echo (($_SERVER["SCRIPT_NAME"] == "/index.php") ? 'class="active"' : '')?>><a href="/">Portail</a></li>
        <li <?php echo (($_SERVER["SCRIPT_NAME"] == "/profile.php") ? 'class="active"' : '')?>><a href="/profile.php">Modifier mon profil</a></li>
        <?php 
          if($_SESSION['user']["gidnumber"][0] == 501 || $_SESSION['user']["gidnumber"][0] == 504):
        ?>
        <li <?php echo ((strpos($_SERVER["SCRIPT_NAME"], "/admin/") !== false) ? 'class="active dropdown"' : 'class="dropdown"')?>>
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Admin <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li <?php echo (($_SERVER["SCRIPT_NAME"] == "/admin/accessCode.php") ? 'class="active"' : '')?>><a href="/admin/accessCode.php">Code d'accès</a></li>
            <li role="separator" class="divider"></li>
            <li <?php echo (($_SERVER["SCRIPT_NAME"] == "/admin/mailLanding.php") ? 'class="active"' : '')?>><a href="/admin/mailLanding.php">Gestion mail landing page</a></li>
          </ul>
        </li>
        <?php 
          endif;
        ?>
        <li><a href="/logout.php">Déconnexion</a></li>
      </ul>
    </div><!--/.nav-collapse -->
  </div>
</nav>
